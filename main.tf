provider "aws" {
  region     = var.region
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
}

resource "aws_s3_bucket" "bucket" {
  bucket = var.bucket_name

  tags = {
    Name = var.bucket_name
  }

  versioning {
    enabled = var.versioning_enabled
  }

  logging {
    target_bucket = var.logging_bucket
    target_prefix = var.bucket_name
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}

resource "aws_s3_bucket_policy" "bucket_policy" {
    bucket = aws_s3_bucket.bucket.id

    policy = jsonencode({
        "Statement": [
            {
                "Action": "s3:*",
                "Effect": "Deny",
                "Resource": [
                    join("", ["arn:aws:s3:::", var.bucket_name]),
                    join("", ["arn:aws:s3:::", var.bucket_name, "/*"])
                ],
                "Principal": "*",
                "Condition": {
                    "Bool": {
                        "aws:SecureTransport": "false"
                    }
                }
            }
        ]
    })
}