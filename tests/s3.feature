Feature: S3 Feature

    S3 Checks

Scenario: S3 Encryption
    Given I have AWS S3 Bucket defined
    Then it must contain server_side_encryption_configuration

Scenario: Logging Enabled
    Given I have AWS S3 Bucket defined
    Then it must contain logging