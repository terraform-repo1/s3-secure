# s3-secure

Terraform module that creates an S3 bucket with:
* Default encryption (AES256) enabled
* Server access logging enabled
* A policy that Denies non-SSL requests


## Variables
The following variables are required:

* "region" - the region in which to create the bucket.
* "bucket_name" - the name of the bucket (this must be globally unique).
* "versioning_enabled" - a flag to enable/disable versioning (optional - defaults to true if not specified).
* "logging_bucket" - the name of the bucket that the server access logs will be written to.
