variable "region" {
  type = string
}

variable "bucket_name" {
  type = string
}

variable "versioning_enabled" {
  type    = string
  default = true
}

variable "logging_bucket" {
  type = string
}

variable "aws_access_key" {
  type = string
}

variable "aws_secret_key" { 
  type = string
}
